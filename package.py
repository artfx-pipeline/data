name = "data"

version = "1.0.1"

authors = ['ArtFx TD gang']

description = \
    """
    Data is a interface for data source (filesystem, bdd, shotgun ...)
    """

requires = [
    "python",
    "filesystem",
]

vcs = "git"

def commands():
    global env
    env.PATH.append("{root}/lib")
