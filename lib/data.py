# -*- coding: utf-8 -*-

"""

Data manager

"""

import file_system 

"""
Data from filesystem
"""

def get_childs(data):
    """
    Get childs return the next conform directories from data
    See filesystem for more info
    :param data: Data from a valid path (can be a valid conform path)
    :return: a generator of data (dictionary)
    """
    try:
        file_system.get_childs(data)
    except Exception as e:
        raise e


"""
Data from bdd
"""


"""
Data syncronisation
"""

